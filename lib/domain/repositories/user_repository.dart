import 'package:dartz/dartz.dart';
import 'package:kuhoo_flutter_assignment/domain/entities/user_entity.dart';

abstract class UserRepository {
  Future<Either<String, List<User>>> fetchUsers(int page);
  Future<Either<String, User>> fetchUserDetails(int userId);
}
