import 'package:flutter/material.dart';
import 'package:kuhoo_flutter_assignment/presentation/views/user_detail_page.dart';
import 'package:kuhoo_flutter_assignment/presentation/views/user_list_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'User App',
      debugShowCheckedModeBanner: false,
      initialRoute: '/users',
      routes: {
        '/users': (context) => const UserListPage(),
        '/users/details': (context) => const UserDetailsPage(),
      },
    );
  }
}
