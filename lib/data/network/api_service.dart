import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class DioClient {
  static BaseOptions baseOptions = BaseOptions(
    responseType: ResponseType.json,
    connectTimeout: const Duration(milliseconds: 60000),
    receiveTimeout: const Duration(milliseconds: 60000),
  );
  static final DioClient _dioClient = DioClient._internal();
  final Dio _dio = Dio(baseOptions);

  Dio get dio => _dio;

  factory DioClient() {
    return _dioClient;
  }

  DioClient._internal() {
    if (kDebugMode) {
      _dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
      ));
    }
  }

  static Future<Response<T>> get<T>(String path,
      {Map<String, dynamic>? queryParameters}) async {
    try {
      final response =
          await DioClient().dio.get<T>(path, queryParameters: queryParameters);
      return response;
    } on DioException catch (e) {
      throw DioException(
          message: 'Network Error',
          requestOptions: e.requestOptions,
          response: e.response);
    }
  }

  static Future<Response<T>> post<T>(String path, {dynamic data}) async {
    try {
      final response = await DioClient().dio.post<T>(path, data: data);
      return response;
    } on DioException catch (e) {
      throw DioException(
          message: 'Network Error',
          requestOptions: e.requestOptions,
          response: e.response);
    }
  }
}
