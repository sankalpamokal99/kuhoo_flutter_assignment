import 'package:dartz/dartz.dart';
import 'package:kuhoo_flutter_assignment/data/model/user_list_model.dart';
import 'package:kuhoo_flutter_assignment/data/network/api_service.dart';
import 'package:kuhoo_flutter_assignment/domain/entities/user_entity.dart';
import 'package:kuhoo_flutter_assignment/utils/constants/api_endpoints.dart';

import '../../domain/repositories/user_repository.dart';

class UserRepositoryImpl implements UserRepository {
  @override
  Future<Either<String, List<User>>> fetchUsers(int page) async {
    try {
      final response = await DioClient.get(
          '${ApiEndPoints.BASE_URL}${ApiEndPoints.GET_USER_LIST}',
          queryParameters: {'page': page});

      if (response.statusCode == 200) {
        UserListModel userListModel = UserListModel.fromJson(response.data);
        List<User> users = userListModel.data ?? [];
        return right(users);
      } else {
        return left("Error");
      }
    } catch (e) {
      return left("Error");
    }
  }

  @override
  Future<Either<String, User>> fetchUserDetails(int userId) async {
    try {
      final response = await DioClient.get(
          '${ApiEndPoints.BASE_URL}${ApiEndPoints.GET_USER_LIST}/$userId');

      if (response.statusCode == 200) {
        User user = User.fromJson(response.data['data']);
        return right(user);
      } else {
        return left("Error");
      }
    } catch (e) {
      return left("Error");
    }
  }
}
