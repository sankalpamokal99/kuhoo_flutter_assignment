import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kuhoo_flutter_assignment/data/repositories/user_repository_impl.dart';
import 'package:kuhoo_flutter_assignment/domain/entities/user_entity.dart';
import 'package:kuhoo_flutter_assignment/domain/repositories/user_repository.dart';

class UserListBloc extends Bloc<UserListEvent, UserListState> {
  final UserRepository userRepository = UserRepositoryImpl();
  ScrollController controller = ScrollController();
  int page = 1;
  int limit = 6;
  List<User> usersList = List.empty(growable: true);
  bool hasMoreData = true;
  bool isInitialLoading = true;

  UserListBloc() : super(UserListInitial()) {
    on<FetchUsersEvent>(_onFetchUsersEvent);
    add(FetchUsersEvent(page: page));
    controller.addListener(() {
      if (controller.position.maxScrollExtent == controller.position.pixels) {
        add(FetchUsersEvent(page: page));
      }
    });
  }

  void _onFetchUsersEvent(
      FetchUsersEvent event, Emitter<UserListState> emit) async {
    if (isInitialLoading) {
      emit(UserListLoading());
    }
    try {
      final result = await userRepository.fetchUsers(event.page);
      result.fold((l) {
        emit(UserListError());
      }, (r) {
        if (r.length < 6) {
          hasMoreData = false;
        } else {
          hasMoreData = true;
          page++;
        }
        usersList.addAll(r);
        emit(UserListLoaded(users: usersList));
        isInitialLoading = false;
      });
    } catch (e) {
      emit(UserListError());
    }
  }
}

// UserList Events
abstract class UserListEvent {
  const UserListEvent();
}

class FetchUsersEvent extends UserListEvent {
  final int page;

  FetchUsersEvent({required this.page});
}

// UserList States
abstract class UserListState {}

class UserListInitial extends UserListState {}

class UserListLoading extends UserListState {}

class UserListLoaded extends UserListState {
  final List<User> users;

  UserListLoaded({required this.users});
}

class UserListError extends UserListState {}
