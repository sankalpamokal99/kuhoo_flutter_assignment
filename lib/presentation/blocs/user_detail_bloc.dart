import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kuhoo_flutter_assignment/data/repositories/user_repository_impl.dart';
import 'package:kuhoo_flutter_assignment/domain/entities/user_entity.dart';
import 'package:kuhoo_flutter_assignment/domain/repositories/user_repository.dart';

class UserDetailsBloc extends Bloc<UserDetailsEvent, UserDetailsState> {
  final UserRepository userRepository = UserRepositoryImpl();

  UserDetailsBloc(int userId) : super(UserDetailsInitial()) {
    on<FetchUserDetailsEvent>(_onFetchUserDetailsEvent);
    add(FetchUserDetailsEvent(userId: userId));
  }

  void _onFetchUserDetailsEvent(
      FetchUserDetailsEvent event, Emitter<UserDetailsState> emit) async {
    emit(UserDetailsLoading());
    try {
      final result = await userRepository.fetchUserDetails(event.userId);
      result.fold((l) {
        emit(UserDetailsError());
      }, (r) {
        emit(UserDetailsLoaded(user: r));
      });
    } catch (e) {
      emit(UserDetailsError());
    }
  }
}

// UserList Events
abstract class UserDetailsEvent {
  const UserDetailsEvent();
}

class FetchUserDetailsEvent extends UserDetailsEvent {
  int userId;

  FetchUserDetailsEvent({required this.userId});
}

// States
abstract class UserDetailsState {
  const UserDetailsState();
}

class UserDetailsInitial extends UserDetailsState {}

class UserDetailsLoading extends UserDetailsState {}

class UserDetailsLoaded extends UserDetailsState {
  final User user;

  UserDetailsLoaded({required this.user});
}

class UserDetailsError extends UserDetailsState {}
