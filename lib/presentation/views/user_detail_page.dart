import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kuhoo_flutter_assignment/presentation/blocs/user_detail_bloc.dart';

class UserDetailsPage extends StatelessWidget {
  const UserDetailsPage({super.key});

  @override
  Widget build(BuildContext context) {
    final Map<String, dynamic> args =
        ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
    final int userId = args['userId'];

    return BlocProvider(
      create: (context) => UserDetailsBloc(userId),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('User Detail'),
        ),
        body: UserDetail(),
      ),
    );
  }
}

class UserDetail extends StatelessWidget {
  const UserDetail({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserDetailsBloc, UserDetailsState>(
      builder: (context, state) {
        if (state is UserDetailsLoaded) {
          return Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.network(
                  state.user.avatar ?? "",
                  errorBuilder: (context, _, __) {
                    return const Icon(Icons.error);
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                Text("${state.user.firstName} ${state.user.lastName}"),
                Text("${state.user.email}"),
              ],
            ),
          );
        } else if (state is UserDetailsError) {
          return const Text('Failed to load user details.');
        } else if (state is UserDetailsLoading) {
          return const Center(child: CircularProgressIndicator());
        }
        return const Center(child: Text('Something went wrong'));
      },
    );
  }
}
