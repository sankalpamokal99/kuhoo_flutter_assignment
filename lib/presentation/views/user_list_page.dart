import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kuhoo_flutter_assignment/presentation/blocs/user_list_bloc.dart';

class UserListPage extends StatelessWidget {
  const UserListPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => UserListBloc(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('User List'),
        ),
        body: const UserList(),
      ),
    );
  }
}

class UserList extends StatelessWidget {
  const UserList({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserListBloc, UserListState>(
      builder: (context, state) {
        if (state is UserListLoaded) {
          return ListView.builder(
              controller: BlocProvider.of<UserListBloc>(context).controller,
              itemCount: state.users.length,
              itemBuilder: (context, index) {
                return Card(
                  elevation: 5,
                  margin:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(
                        context,
                        '/users/details',
                        arguments: {'userId': state.users[index].id},
                      );
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Row(
                        children: [
                          Image.network(
                            height: 120,
                            width: 120,
                            state.users[index].avatar ?? "",
                            errorBuilder: (context, _, __) {
                              return const Icon(Icons.error);
                            },
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 20.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    "${state.users[index].firstName} ${state.users[index].lastName}"),
                                Text("${state.users[index].email}"),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              });
        } else if (state is UserListError) {
          return const Text('Failed to load users.');
        } else if (state is UserListLoading) {
          return const Center(child: CircularProgressIndicator());
        }
        return const Text('Something went wrong!');
      },
    );
  }
}
